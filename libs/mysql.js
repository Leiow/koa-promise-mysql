const mysql = require('promise-mysql');

const config = require('../config/config.json');

exports.query = async (sql) => {
  const connection = await mysql.createConnection(config);
  return connection.query(sql);
};

