const Koa = require('koa');
const Router = require('koa-router');
const Bodypasrser = require('koa-bodyparser');
const test = require('./libs/mysql');

const app = new Koa();
const router = new Router();

app.use(Bodypasrser());

router
  .get('/info', (ctx) => {
    ctx.body = 'hello';
  })
  .get('/test1', async (ctx) => {
    let data = await test.test1();
    ctx.body = data;
  })
  .get('/test2', async (ctx) => {
    const data = await test.query('SELECT * FROM `device`');
    console.log(data);
    ctx.body = 'test2';
  })
  .post('/post', (ctx) => {
    console.log(ctx.request.body);
  });

app.use(router.routes());
app.use(router.allowedMethods());

app.listen(3000);
